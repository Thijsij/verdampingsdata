# -*- coding: utf-8 -*-
#
# Thijs IJpelaar, 23-06-2017

from __future__ import division
from pyproj import Proj, transform
from scipy import spatial
import matplotlib.pyplot as plt
import numpy as np
import h5py


#Functie om de meest nabije pixel te vinden voor een waarde
def find_nearest(a, a0):
    idx = np.abs(a - a0).argmin()
    return a.flat[idx],idx


##### INLEZEN #####
#Inlezen x- en y-data:
if 1==0:
    #Lees eLeaf x- en y-data uit database en converteer naar RD
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/eLeaf2016.h5', 'r') as hf:
        x1 = hf['xa'][:]
        y1 = hf['ya'][:]
        
    xi,yi = np.meshgrid(x1,y1)
    xj,yj = xi.flatten('C'),yi.flatten('C')

    IN = Proj(init='epsg:32631')        #WGS 84, lat/lon, transverse mercator 31np
    OU = Proj(init='epsg:28992')        #RD, rijksdriehoek
    xEla,yEla = transform(IN,OU,xj,yj)
        
    xEl = xEla.reshape(1309,1049)
    yEl = yEla.reshape(1309,1049)
    
    #Lees neerslagdata en herprojecteer lat lon
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/Neerslag/neerslag2016_H5.h5', 'r') as hf:
        prec = hf['prec'][:]
        x2 =   hf['lat'][:]
        y2 =   np.flipud(hf['lon'][:])
    
    IN = Proj(init='epsg:4326')         #WGS 84, lat/lon, transverse mercator 31n
    OU = Proj(init='epsg:28992')        #RD, rijksdriehoek
    xPr,yPr = transform(IN,OU,x2,y2)

    #Lees KNMI Eref Makkink uit database, is al in RD
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/Eref_Makkink_KNMI/ErefKNMI2016.h5', 'r') as hf:
        ErM = hf['Er'][:]
        xMa = hf['x'][:]
        yMa = hf['y'][:] 

    #Inlezen data op specifieke punten:    
    #Pak een locatie en zoek naar de pixelwaardes
    #        water,        bebouwing, heide,    naaldbos,  mais,     mais,      grasland,  mais,     grasland,   grasland,  punt zeeland
    locsx = [151973.2,	149227.3,	185274.9,	181212.6,	157843.6,	167111.2,	169124.5,	181984.4,	176942.4,	139824.5,   19549]
    locsy = [411236.7,	411144.5,	374125.03,	394152.4,	422305.6,	422993.9,	423169.1,	388766.9,	379659.5,	414631.1,   395506]

    
    #Even wat moeilijk gedoe om de eLeaf/precipitatie-indices te vinden voor de locatie
    xyarrayE = np.dstack([xEl.ravel(),yEl.ravel()])[0]
    xyarrayP = np.dstack([xPr.ravel(),yPr.ravel()])[0]
    
    tree_E = spatial.cKDTree(xyarrayE)
    tree_P = spatial.cKDTree(xyarrayP)
    
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/eLeaf2016.h5', 'r') as hf:    
        n=0
        shape = (len(locsx),366)
        Eav = np.empty(shape); Edv = np.empty(shape); Eov = np.empty(shape); Eqv = np.empty(shape)
        Pv =  np.empty(shape); Erv = np.empty(shape)
        for locx,locy in zip(locsx,locsy):
        
            points=np.array([locx,locy])
            points_list = list(points.transpose())
            
            #Vind per punt de juist x- en y-waardes voor Ea/Ed, P en ErMakkink            
            dist, indexes = tree_E.query(points_list)
            yEli,xEli = np.where((xEl==xyarrayE[indexes][0])==True) #Gaat iets fout bij latlon
            dist, indexes = tree_P.query(points_list)
            yPri,xPri = np.where((xPr==xyarrayP[indexes][0])==True)
            
            a,xMai = find_nearest(xMa,locx) #Mag je zo doen, kolommen zijn gelijk
            a,yMai = find_nearest(yMa,locy)
            
            #Lees data uit op locatie voor Ea eLeaf, [mm/day]
            Eav[n] = hf['Ea'][yEli,xEli,:]
            Edv[n] = hf['Ed'][yEli,xEli,:]
            Eov[n] = hf['Eo'][yEli,xEli,:]
            Eqv[n] = hf['Eq'][yEli,xEli,:]

            if n==0:
                A = hf['Ea'][:,:,100]
            
            #Lees data uit op locatie voor neerslag [mm/day]
            Pv[n]  = prec[yPri,xPri,0:prec.shape[2]] #Let op, prec is gekanteld
            Pv[n][Pv[n]>=655]=0
            
            #Lees data uit op locatie voor Er Makkink, [kg m-2]
            Erv[n] = ErM[yMai,xMai,:]
            print yEli,xEli
            n=n+1


    #Inlezen cropfactors en berekening Ep
    with open('/home/baracus/Documents/Werk/Verdampingsdata/Data/CROPFACT', 'r') as cf:
        lines =  cf.readlines()
        
    #0    1     2   3     4    5        6         7     8             9               10              11      12             13             14          15     16     17         18
    #Year,Month,Day,grass,corn,potatoes,sugarbeet,grain,miscellaneous,non-arable land,greenhouse area,orchard,bulbous plants,foliage forest,pine forest,nature,fallow,vegetables,flowers
    cfs = np.empty([366,19])
    for i in range(20,386):
        cfs[i-20,:] = np.array(lines[i].split())
    
    Epv = np.empty([11, 366])
    #        water,        bebouwing, heide,    naaldbos,  mais,     mais,      grasland,  mais,     grasland,   grasland,  punt zeeland
    column = [18,          18,        18,       14,        4,        4,         3,         4,        3,          3,         18]
    for j in range(0,11):
        Epv[j,:] = Erv[j,:] * cfs[:,column[j]]


##### PLOTTEN #####
figpath = '/home/baracus/Documents/Werk/Verdampingsdata/Figures/'
#subplot samenvatting
if 1==1:
    
    for p in range(0,15):
        #plt.ioff()
        
        fig1, arr = plt.subplots(4,1)
        titles = ['Water Noord','Bebouwing Noord','Heide Zuid','Naaldbos Zuid','Mais A Noord','Mais B Noord','Grasland A Noord','Mais Zuid','Grasland Zuid','Grasland B Noord','Zeeland']
        labels = ['Jan', 'Feb', 'Mrt', 'Apr','Mei','Jun','Jul','Aug','Sep','Okt','Nov','Dec']
        M=[0,31,29,31,30,31,30,31,30,31,30,31,30]
        
        arr[0].plot(Erv[p,:],'dimgray',label='Er',linewidth=1,linestyle=':')
        if p in [3,4,5,6,7,8,9,10]:
            arr[0].plot(Epv[p,:],'dodgerblue',label='Ep',linewidth=1)
        arr[0].plot(Eav[p,:],'orangered',label='Ea',linewidth=1)
        arr[0].set_ylabel('[mm]')
        arr[0].legend(fontsize = 'smaller', labelspacing=0,frameon=False)
        arr[0].set_title(titles[p],size=15)
        arr[0].set_xlim([0,365])
        arr[0].set_xticks(np.cumsum(M))
        arr[0].set_xticklabels(labels)

        arr[1].plot(Edv[p], 'orangered',label='Ed')
        arr[1].set_ylabel('Ed [mm]')
        arr[1].set_ylim([0,max(Edv[p])*1.1])
        arr[1].set_xlim([0,365])
        arr[1].set_xticks(np.cumsum(M))
        arr[1].set_xticklabels(labels)
    
        ax1 = arr[1].twinx()
        ax1.bar(np.arange(Pv[p,:].shape[0]), Pv[p,:], 1, color="dodgerblue",label='Precipitatie')
        ax1.set_ylabel('[mm]')
        ax1.set_ylim([0,max(Pv[p,:])*1.1])
        ax1.plot(0,0, 'orangered',label='Ed')
        ax1.legend(fontsize = 'smaller', labelspacing=0,frameon=False)
        ax1.set_xlim([0,366])
        ax1.set_xticks(np.cumsum(M))
        ax1.set_xticklabels(labels)
        
        #Observaties
        meting = np.empty(366)
        for i in range(0,len(Eov[p])):
            if Eov[p][i] == 0:
                meting[i] = 1
            else:
                meting[i] = 0
        arr[2].plot(Eov[p],color='orangered',label='Dagen zonder observaties',linewidth=2)
        arr[2].bar(np.arange(0,366), meting*100, 1, color="dodgerblue",label='Satellietmeting',alpha=0.5)
        arr[2].set_ylim((0,int(np.max(Eov[p])*1.1)))
        arr[2].set_xlim((0,365))
        arr[2].set_ylabel('Dagen zonder observaties')
        #arr[2].legend(fontsize = 'smaller', labelspacing=0,frameon=True, loc=1)
        
        ax2 = arr[2].twinx()
        ax2.plot(Eqv[p], 'dimgray',label='Kwaliteitsparameter',linewidth=2,linestyle=':')
        ax2.set_ylabel('Kwaliteitsparameter')
        ax2.set_ylim([0,1])
        ax2.set_ylim(ax2.get_ylim()[::-1])
        ax2.plot(0,0, color='orangered',label='Dagen zonder observaties',linewidth=2)
        ax2.plot(0,0, color="dodgerblue",label='Satellietmeting',alpha=0.5)
        ax2.legend(fontsize = 'smaller', labelspacing=0,frameon=True,loc=1)
        ax2.set_xlim([0,366])
        ax2.set_xticks(np.cumsum(M))
        ax2.set_xticklabels(labels)
        
        ErCS = np.cumsum(Erv[p,:])
        EaCS = np.cumsum(Eav[p,:])
        EpCS = np.cumsum(Epv[p,:])
        arr[3].plot(ErCS,'dimgray',label='Er',linewidth=2,linestyle=':')
        if p in [3,4,5,6,7,8,9,10]:
            arr[3].plot(EpCS,'dodgerblue',label='Ep',linewidth=2)
        arr[3].plot(EaCS,'orangered',label='Ea')
        arr[3].set_ylabel('[mm]')
        arr[3].legend(fontsize = 'smaller', labelspacing=0,frameon=False)
        arr[3].set_xlim([0,365])
        arr[3].set_xticks(np.cumsum(M))
        arr[3].set_xticklabels(labels)
              
        fig1.set_size_inches(15, 12)
        fig1.tight_layout()
        
        plt.show()
        plt.savefig(figpath+'punt'+str(p)+'obs.png',format='png', dpi=300)
        plt.close()
        
