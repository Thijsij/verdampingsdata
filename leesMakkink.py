# -*- coding: utf-8 -*-
#
# Thijs IJpelaar, 23-06-2017

from __future__ import division
import numpy as np
from netCDF4 import Dataset
import os
import h5py 


#Aan te pasen variabelen
#Invoerpaden:
basepath = '/home/baracus/Documents/Werk/Verdampingsdata/Data/Eref_Makkink_KNMI/Brondata/'
#Uitvoerpaden:
fui = '/home/baracus/Documents/Werk/Verdampingsdata/Data/Eref_Makkink_KNMI/ErefKNMI2016.h5'
#Kies een jaartal om te bepalen
jaar = '2016'


n=0
Er = np.empty([350, 300,366])
for year in sorted(os.listdir(basepath)):
    #Bepaal alleen voor een bepaald jaar
    if year == jaar:
        for month in sorted(os.listdir(basepath+year)):
            for day in sorted(os.listdir(basepath+year+'/'+month)):
                for doc in sorted(os.listdir(basepath+year+'/'+month+'/'+day)):
                    if doc.endswith('.nc'):
                        fn = basepath+year+'/'+month+'/'+day+'/'+doc
                        fh = Dataset(fn,mode='r')  
                        
                        x   = fh.variables['x'][:]
                        y   = fh.variables['y'][:]
                        Er[:,:,n]  = fh.variables['prediction'][:][0,:,:]
                        n=n+1
                        
                        fh.close()
                        
                    else:
                        print "BESTAND EINDIGT NIET IN .NC, gaat iets fout!"
            print month

#Uitschrijven naar h5
with h5py.File(fui, 'w') as hf:
    hf.create_dataset("Er",  data=Er)
    hf.create_dataset("x",  data=x)
    hf.create_dataset("y",  data=y)
    
    
    