* Voorwoord
Dit is een kort begeleidend document opgesteld om de structuur van de Pythonscripts in deze folder en de algemene verwerking van de verdampingsdata te borgen. Deze scripts zijn in het voorjaar van 2017 gebruikt in een validatiestudie van door eLeaf aangeleverde verdampingsgrids van Nederland. De scripts zelf zullen niet voorzien worden van een header, de relante informatie zal te vinden zijn in dit document.
Voor vragen en informatie kan je contact opnemen met Thijs IJpelaar, tijpelaar@aaenmaas.nl.

* Inleiding
In de validatiestudie worden enkele datasets gebruikt om de verdampingsdata te valideren:

   - eLeaf verdampingsproduct [netCDF, dagbasis]
	#   Ea - Actuele verdamping
	#   Ed - Verdampingstekort
	#   Eo - Het aantal dagen zonder observaties
	#   Eq - De kwaliteitsparameter van eLeaf    
   - KNMI neerslagdata [h5, dagbasis] (https://data.knmi.nl/datasets/radar_corr_accum_24h/1.0?q=neerslag,radar)
   - KNMI Makkink referentie gewasverdamping [netCDF, dagbasis] (https://data.knmi.nl/datasets/EV24/2?q=makkink)
   - Cabauw verdampingsdata [netCDF, maandbasis] (http://www.cesar-database.nl/ShowDatasetMetadataInfo.do?datasetMetadataID=1159)
   - Loobos verdampingsdata [XLSX, dagbasis] (E-mailcorrespondentie Jan Elbers, WUR)



* Scripts
Om verwerking te vergemakkelijken worden alle bronbestanden uitgelezen en opgeslagen in één enkel h5-bestand per onderdeel. Het voordeel van dit h5-bestand is dat het data op jaarbasis is en array-style slicing toestaat, wat bewerking en visualisatie vergemakkelijkt. Dit gebeurt met de volgende scripts:


leesEleaf.py	
leesMakkink.py
leesNeerslag.py

N.B.: zorg dat je de juiste libraries hebt en zorg dat je alle paden aanpast! Alle relevante aan te passen variabelen worden aan het begin van het document genoemd.

Om de ingelezen data te plotten worden de h5-bestanden opnieuw geopend in plot-scripts en wordt de data geplot.

plotInSitu.py 		-> Plot Cabauw- en Loosbosdata
plotMaandsommen.py	-> Plot jaar- en maandsommen van Nederland en de inzoomkaartjes Nijmegen/Friesland
plotPunt.py		-> Plot samenvattende informatie op puntlocaties
plotQuality.py		-> Plot kwaliteitsegevens

Deze scripts zijn meestal opgedeeld in onderdelen, beginnend met een if-statement: if 1==1:
De scripts zijn opgedeeld in twee secties, een inleessectie en een plotsectie. Inlezen kost tijd en vergt veel geheugen, doe dit dan ook éénmaal en gebruik de in het geheugen ingeladen data om te plotten. Pas netjes alle paden in het héle script aan.

   - Thijs IJpelaar


