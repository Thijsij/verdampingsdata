# -*- coding: utf-8 -*-
#
# Thijs IJpelaar, 23-06-2017

from __future__ import division
from pyproj import Proj, transform
from scipy import spatial
from netCDF4 import Dataset
import matplotlib.pyplot as plt
import numpy as np
import h5py
import csv
import os


#Functie om de meest nabije pixel te vinden voor een waarde
def find_nearest(a, a0):
    idx = np.abs(a - a0).argmin()
    return a.flat[idx],idx


##### INLEZEN #####
#Inlezen x- en y-data:
if 1==0:
    #Lees eLeaf x- en y-data uit database van 2016 en converteer naar RD
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/eLeaf2016.h5', 'r') as hf:
        x1 = hf['xa'][:]
        y1 = hf['ya'][:]
        
    xi,yi = np.meshgrid(x1,y1)
    xj,yj = xi.flatten('C'),yi.flatten('C')

    IN = Proj(init='epsg:32631')        #WGS 84, lat/lon, transverse mercator 31n
    OU = Proj(init='epsg:28992')        #RD, rijksdriehoek
    xEla,yEla = transform(IN,OU,xj,yj)
        
    xEl = xEla.reshape(1309,1049)
    yEl = yEla.reshape(1309,1049)

    #Lees KNMI Eref Makkink uit database van 2016, is al in RD
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/Eref_Makkink_KNMI/ErefKNMI2016.h5', 'r') as hf:
        ErM16 = hf['Er'][:]
        xMa = hf['x'][:]
        yMa = hf['y'][:] 

    #Lees KNMI Eref Makkink uit database van 2006, is al in RD
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/Eref_Makkink_KNMI/ErefKNMI2006.h5', 'r') as hf:
        ErM06 = hf['Er'][:]
    
    
    #Even wat moeilijk gedoe om de eLeaf/precipitatie-indices te vinden voor de locatie
    #Locaties van meetpunten
    #   ['cabauw.csv',       'loobos.csv',      'lutjewad.csv',      'vredepeel.csv']
    x = [123320.37496013576, 179380.38597087687, 219443.17673401066, 179727.29856268462]
    y = [442522.78509915574, 464329.2694753177,  601834.0911361049,  393610.78310629295]
    xyarrayE = np.dstack([xEl.ravel(),yEl.ravel()])[0]    
    tree_E = spatial.cKDTree(xyarrayE)
    
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/eLeaf2016.h5', 'r') as hf:    
        n=0
        shape = (len(x),366)        
        Eav = np.empty(shape); Edv = np.empty(shape); Eov = np.empty(shape); Eqv = np.empty(shape)
        Erv16 = np.empty(shape)   
        Erv06 = np.empty(shape)   
        
        for locx,locy in zip(x,y):

            #Lees data uit op locatie voor Ea eLeaf, [mm/day]
            points=np.array([locx,locy])
            points_list = list(points.transpose())
            dist, indexes = tree_E.query(points_list)
            yEli,xEli = np.where((xEl==xyarrayE[indexes][0])==True) #Gaat iets fout bij latlon            
            Eav[n] = hf['Ea'][yEli,xEli,:]
            Edv[n] = hf['Ed'][yEli,xEli,:]
            Eov[n] = hf['Eo'][yEli,xEli,:]
            Eqv[n] = hf['Eq'][yEli,xEli,:]
            
            if n==0:
                test = hf['Ea'][:,:,150]
                test[test<-1]=0
                        
            #Lees data uit op locatie voor Er Makkink, [kg m-2]
            a,xMai = find_nearest(xMa,locx) #Mag je zo doen, kolommen zijn gelijk
            a,yMai = find_nearest(yMa,locy)
            Erv16[n] = ErM16[yMai,xMai,:]
            Erv06[n] = ErM06[yMai,xMai,:]
            
            print yEli,xEli, xMai, yMai            
            n=n+1
    
    #Lees InSitu metingen STOWA 2006
    basepath = '/home/baracus/Documents/Werk/Verdampingsdata/Data/InSituMetingen/STOWA/'
    datasets = ['cabauw.csv','loobos.csv','lutjewad.csv','vredepeel.csv']
    tmax2006 = []
    t = np.empty([4,366])
    InSituStowa = np.empty([4,366])
    for i in range(0,4):
        #Lees in data
        j = 0
        path = basepath + datasets[i]
        with open(path, 'r') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                InSituStowa[i,j] = row[1]
                j=j+1
            tmax2006.append([row[0],j])

    #Lees InSitu metingen Loobos 2016
    basepath = '/home/baracus/Documents/Werk/Verdampingsdata/Data/InSituMetingen/Loobos2016/loobos.csv'
    j = 0
    InSituLoobos2016 = np.empty(366)
    with open(basepath, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            if not j==0: #skip header
                InSituLoobos2016[j-1] = row[1]
            j=j+1
     

    #Lees InSitu metingen Cabauw 2016
    basepath = '/home/baracus/Documents/Werk/Verdampingsdata/Data/InSituMetingen/Cabauw2016/'
    k=0
    InSituCabauw2016 = np.empty(366)
    for doc in sorted(os.listdir(basepath)):
        fh = Dataset(basepath+doc,mode='r')
        LE = fh.variables['LE'][:]
        for i in range(0,int(len(LE)/144)):                                     #0 tot 29/30/31, sommeer dagwaardes (144 waardes per dag)
            InSituCabauw2016[k] = sum(LE[i*144:(i+1)*144]) / 144 / 28.4        #Dagsommen LE / 144 / 28.4
            k = k+1
        fh.close()
    InSituCabauw2016[InSituCabauw2016<-1]=0
        
        #Grootheid is LE
        #Deze data zijn beschikbaar op 10 minuut basis, en in energie termen (W/m2) . 
        #Deze waarden zijn alleen betrouwbaar op daggemiddelde basis. 
        #Dit komt omdat een aantal warmte opslag termen (vegetatie en top van de minerale bodem) die op 
        #sub-dag basis spelen niet nauwkeurig worden meegenomen in het budget. 
        #Op dag basis tellen deze termen op tot ongeveer nul. 
        #Om tot dagwaarden te komen moet je de 144 waarden middelen. 
        #Om vervolgens tot hoeveelheid te komen (mm/dag) moet je nog delen door 28.4 (W/m2)/(mm/dag).

#Samenvatting:
     #Er 2006:          Erv06
     #Er 2016:          Erv16
     #Ea 2016 eLeaf:    Eav, Edv
     #Ea 2006 STOWA:    InSituStowa
     #Ea 2016 Loobos:   InSituLoobos2016
     #Ea 2016 Cabauw:   InSituCabauw2016
     
     #Cabauw: 365 dagen
     #Loobos: 365 dagen
     #Lutjewad: 236 dagen,  vanaf 5/10/2006, 366-(31+29+31+30+10) = 235     131
     #Vredepeel: 283 dagen, vanaf 3/24/2006, 366-(31+29+24) = 282           84


##### PLOTTEN #####
figpath = '/home/baracus/Documents/Werk/Verdampingsdata/Figures/'
#Plot data 2016 Loobos
if 1==1:
    for p in range(0,2):
        plt.ioff()    
        titles = ['Cabauw','Loobos']
        labels = ['Jan', 'Feb', 'Mrt', 'Apr','Mei','Jun','Jul','Aug','Sep','Okt','Nov','Dec']
        M=[0,31,29,31,30,31,30,31,30,31,30,31,30]

        fig1, arr = plt.subplots(3,1)
        
        InSituData = [InSituCabauw2016,InSituLoobos2016]
        SatData    = [Eav[0,:],Eav[1,:]] #0 correspondeert met Cabauw, 1 met Loobos
        ObsData    = [Eov[0,:],Eov[1,:]]
        
        #2016 InSitu  
        arr[0].plot(InSituData[p] ,'dodgerblue',  label='Ea meting')
        arr[0].plot(SatData[p]    ,'orangered', label='Ea eLeaf')                     
        arr[0].set_xlim([0,365])
        arr[0].set_ylabel('[mm]')
        arr[0].legend(fontsize = 'smaller', labelspacing=0,frameon=False)
        arr[0].set_title(titles[p])
        arr[0].set_xticks(np.cumsum(M))
        arr[0].set_xticklabels(labels)

        #2016 InSitu  - eLeaf
        arr[1].plot([0,366],[0,0],'black')
        arr[1].plot(InSituData[p] - SatData[p],'orangered',label='Ea - eLeaf')
        arr[1].set_xlim([0,365])
        arr[1].set_ylabel('[mm]')
        #arr[1].set_title('Ea '+titles[p]+' - Ea eLeaf',size=12,y=0.9,x=0.01,ha='left')
        arr[1].legend(fontsize = 'smaller', labelspacing=0,frameon=False)
        arr[1].set_xticks(np.cumsum(M))
        arr[1].set_xticklabels(labels)
        
        meting = np.empty(366)
        for i in range(0,len(Eov[p])):
            if Eov[p][i] == 0:
                meting[i] = 1
            else:
                meting[i] = 0        
        
        #2016 eLeaf no observations
        arr[2].plot(ObsData[p] ,color='orangered',label='Dagen zonder observaties')
        arr[2].bar(np.arange(0,366), meting*100, 1, color="dodgerblue",label='Satellietmeting',alpha=0.5)
        arr[2].set_xlim([0,365])
        arr[2].set_ylabel('Dagen')
        #arr[2].legend(fontsize = 'smaller', labelspacing=0,frameon=True)
        #arr[2].set_xticks(np.cumsum(M))
        #arr[2].set_xticklabels(labels)
        arr[2].set_ylim(0,15)
        
        ax2 = arr[2].twinx()
        ax2.plot(Eqv[p], 'dimgray',label='Kwaliteitsparameter',linewidth=2,linestyle=':')
        ax2.set_ylabel('Kwaliteitsparameter')
        ax2.set_ylim([0,1])
        ax2.set_ylim(ax2.get_ylim()[::-1])
        ax2.plot(0,0, color='orangered',label='Dagen zonder observaties',linewidth=2)
        ax2.plot(0,0, color="dodgerblue",label='Satellietmeting',alpha=0.5)
        ax2.legend(fontsize = 'smaller', labelspacing=0,frameon=True,loc=1)
        ax2.set_xlim([0,366])
        ax2.set_xticks(np.cumsum(M))
        ax2.set_xticklabels(labels)

        fig1.set_size_inches(14, 8)
        fig1.tight_layout()

        #plt.show()
        #plt.savefig(figpath+str(titles[p])+'.png',format='png', dpi=300)

