# -*- coding: utf-8 -*-
#
# Thijs IJpelaar, 23-06-2017

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import h5py
import sys

def montly_sum(S_MATRIX, V_MATRIX):
    i=0
    M=[31,29,31,30,31,30,31,30,31,30,31,30]
    for n in range(0,12):
        for m in range(i,i+M[n]):
            S_MATRIX[:,:,n] = S_MATRIX[:,:,n] + V_MATRIX[:,:,m]
        i = i+M[n]
    return S_MATRIX


##### INLEZEN #####
#Lees observatiedata uit
if 1==0:
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/eLeaf2016.h5', 'r') as hf:
        Eo  = hf['Eo'][:]
        x1 = hf['xa'][:]
        y1 = hf['ya']
        print "Eo ingelezen"
    
    Eosum = np.empty([1309, 1049,12])
    Eosum = montly_sum(Eosum,Eo)
    Eosum[Eosum<=-1]=None
    
    #Jaarlijkse som
    Eosumy = np.empty([1309, 1049])
    for n in range(0,12):
        Eosumy = Eosumy + Eosum[:,:,n]

    #Lees kwaliteitsdata uit
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/eLeaf2016.h5', 'r') as hf:
        Eq  = hf['Eq'][:]
        x1 = hf['xa'][:]
        y1 = hf['ya']
        print "Eq ingelezen"
        
    Eqsum = np.empty([1309, 1049,12])
    Eqsum = montly_sum(Eqsum,Eq)
    Eqsum[Eqsum<=-1]=None
    
    #Jaarlijkse som
    Eqsumy = np.empty([1309, 1049])
    for n in range(0,12):
        Eqsumy = Eqsumy + Eqsum[:,:,n]

##### PLOTTEN #####        
figpath = '/home/baracus/Documents/Werk/Verdampingsdata/Figures/'
#Histogram
if 1==0:
    print sys.getrefcount(Eo)    
    Eor = np.copy(Eo[:,:,:])    
    print sys.getrefcount(Eo)
    Eorav=np.ravel(Eor)
    print sys.getrefcount(Eo)
    fig1, ax1 = plt.subplots(figsize=(10,10))
    n, bins, patches = plt.hist(Eorav, 75,range=(0,75))
    plt.yscale('log', nonposy='clip')
    plt.ylim((0,11**8))
    plt.tick_params(axis='both', which='major', labelsize=16)
    ax1.set_xlabel('Dagen zonder observaties',fontsize=18)
    plt.title('Histogram dagen zonder observaties, 2016', fontsize=22)
    plt.tight_layout()        
    plt.savefig(figpath+'hist_1.png',format='png', dpi=300)
    
#Plot maximale waardes
if 1==0:
    fig1, ax1 = plt.subplots(figsize=(10,10))
    
    #Eomax = Eq.max(2)
    #Eomax = Eo[:,:,90:305].max(2)
    #Eomax[Eomax<=-998]=None
    imgplot = plt.imshow(Eomax)    
    cb = plt.colorbar()
    plt.clim(0,30)
    cb.ax.tick_params(labelsize=18)
    imgplot.set_cmap('RdYlGn')   
    ax1.set_ylabel('', color='b')
    ax1.set_xlabel('')
    ax1.axes.get_xaxis().set_ticks([])
    ax1.axes.get_yaxis().set_ticks([])
    plt.title('Max. dagen zonder observaties, groeiseizoen', fontsize=22)
    plt.tight_layout()   
    #plt.savefig(figpath+'mdo_groei.png',format='png', dpi=300)
    
#Plot dagen zonder observaties met kwaliteitsparameters
if 1==1:
    labels = ['Jan', 'Feb', 'Mrt', 'Apr','Mei','Jun','Jul','Aug','Sep','Okt','Nov','Dec']
    M=[0,31,29,31,30,31,30,31,30,31,30,31,30]

    fig1, ax1 = plt.subplots(figsize=(12,3))
    meting = np.empty(366)
    for i in range(0,len(Eo[839,658])):
        if Eo[839,658][i] == 0:
            meting[i] = 1
        else:
            meting[i] = 0
            
    plt.plot(Eo[839,658],color='orangered',label='Dagen zonder observaties')
    #plt.plot(meting)
    plt.bar(np.arange(0,366), meting*100, 1, color="dodgerblue",label='Satellietmeting',alpha=0.5)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.ylim((0,21))
    plt.xlim((0,365))
    ax1.set_xticks(np.cumsum(M))
    ax1.set_xticklabels(labels)

    ax2 = ax1.twinx()
    ax2.plot(Eq[839,658], 'dimgray',label='Kwaliteitsparameter',linewidth=2,linestyle=':')
    ax2.set_ylabel('Kwaliteitsparameter')
    ax2.set_ylim([0,1])
    ax2.set_ylim(ax2.get_ylim()[::-1])
    ax2.plot(0,0, color='orangered',label='Dagen zonder observaties',linewidth=2)
    ax2.plot(0,0, color="dodgerblue",label='Satellietmeting',alpha=0.5)
    ax2.legend(fontsize = 'smaller', labelspacing=0,frameon=True,loc=0)
    ax2.set_xlim([0,366])
    ax2.set_xticks(np.cumsum(M))
    ax2.set_xticklabels(labels)

    plt.legend()
    plt.title('Dagen dagen zonder observaties, 2016', fontsize=16)
    plt.tight_layout()   
    plt.savefig(figpath+'dzo.png',format='png', dpi=300)

