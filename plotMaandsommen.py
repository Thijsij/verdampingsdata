# -*- coding: utf-8 -*-
#
# Thijs IJpelaar, 23-06-2017

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import h5py
from osgeo import gdal
from osgeo import osr
from pyproj import Proj, transform
from mpl_toolkits.basemap import Basemap


def montly_sum(S_MATRIX, V_MATRIX):
    i=0
    M=[31,29,31,30,31,30,31,30,31,30,31,30]
    for n in range(0,12):
        for m in range(i,i+M[n]):
            S_MATRIX[:,:,n] = S_MATRIX[:,:,n] + V_MATRIX[:,:,m]
        i = i+M[n]
    return S_MATRIX

def weekly_sum(S_MATRIX, V_MATRIX):
    W=np.arange(198,233,7)
    for n in range(5):
        S_MATRIX[:,:,n] = np.sum(V_MATRIX[:,:,W[n]:W[n]+6],2)
    return S_MATRIX
    
    #export_tiff(data,x1,y1,'Export/Er_mei.tif')
def export_tiff(array,x,y,epsg,path):
    xmin,ymin,xmax,ymax = [x.min(),y.min(),x.max(),y.max()]
    nrows,ncols = np.shape(array)
    xres = (xmax-xmin)/float(ncols)
    yres = (ymax-ymin)/float(nrows)
    geotransform=(xmin,xres,0,ymax,0, -yres)   
    # That's (top left x, w-e pixel resolution, rotation (0 if North is up), 
    #         top left y, rotation (0 if North is up), n-s pixel resolution)
    # I don't know why rotation is in twice???
    
    output_raster = gdal.GetDriverByName('GTiff').Create(path,ncols, nrows, 1 ,gdal.GDT_Float32)  # Open the file
    output_raster.SetGeoTransform(geotransform)  # Specify its coordinates
    srs = osr.SpatialReference()                 # Establish its coordinate encoding
    srs.ImportFromEPSG(epsg)                     # This one specifies WGS84 lat long.
    #srs.ImportFromEPSG(32631)                     # This one specifies WGS84 lat long.

    output_raster.SetProjection( srs.ExportToWkt() )   # Exports the coordinate system 
                                                       # to the file
    output_raster.GetRasterBand(1).WriteArray(array)   # Writes my array to the rasters
    output_raster= None
    
    
##### INLEZEN #####
#Lees data uit op locatie voor Ea eLeaf, [mm/day]
if 1==0:
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016 new/eLeaf2016.h5', 'r') as hf:
        Ea  = hf['Ea'][:]
        x1 = hf['xa'][:]
        y1 = hf['ya'][:]
        print "Ea ingelezen"
        
    Easum = np.empty([1309, 1049,12])
    Easum = montly_sum(Easum,Ea)
    Easum[Easum<=-1]=None
    
    Eaweek = np.empty([1309, 1049,5])
    Eaweek = weekly_sum(Eaweek,Ea)
    Eaweek[Eaweek<=-1]=None
    
    #Jaarlijkse som
    Easumy = np.empty([1309, 1049])
    for n in range(0,12):
        Easumy = Easumy + Easum[:,:,n]

#Lees data uit op locatie voor Ed eLeaf, [mm/day]
if 1==0:      
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016 new/eLeaf2016.h5', 'r') as hf:
        Ed  = hf['Ed'][:]
        x1 = hf['xa'][:]
        y1 = hf['ya'][:]
        print "Ed ingelezen"
    
    Edsum = np.empty([1309, 1049,12])
    Edsum = montly_sum(Edsum,Ed)
    Edsum[Edsum<=-1]=None
    
    Edweek = np.empty([1309, 1049,5])
    Edweek = weekly_sum(Edweek,Ed)
    Edweek[Edweek<=-1]=None
    
    #Jaarlijkse som
    Edsumy = np.empty([1309, 1049])
    for n in range(0,12):
        Edsumy = Edsumy + Edsum[:,:,n]

    #Lees KNMI neerslagdata uit database, is al in RD
    #with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/Neerslag/neerslag2016_H5.h5', 'r') as hf:
    #    prec =  hf['prec'][:]
    #    
    #prec = np.flipud(np.rot90(prec,1))

if 1==1:  
    #Lees KNMI Eref Makkink uit database, is al in RD
    with h5py.File('/home/baracus/Documents/Werk/Verdampingsdata/Data/Eref_Makkink_KNMI/ErefKNMI2016.h5', 'r') as hf:
        ErM =  hf['Er'][:]
        x1m = hf['x'][:]
        y1m = hf['y'][:]
        print "Eref ingelezen"
        
    Emsum = np.empty([350,300,12])  
    Emsum = montly_sum(Emsum,ErM)

    Emweek = np.empty([350,300,5])
    Emweek = np.flipud(weekly_sum(Emweek,ErM))
    Emweek[Emweek<=-1]=None

    #Jaarlijkse som
    Emsumy = np.empty([350, 300])
    for n in range(0,12):
        Emsumy = Emsumy + Emsum[:,:,n]

    #Neerslagsom
    #Psum = np.ndarray(shape=(70,133))
    #for n in range(0,365):
    #    Psum = Psum + prec[:,:,n]


##### PLOTTEN #####
figpath = '/home/baracus/Documents/Werk/Verdampingsdata/Figures/'
#Plots maandsommen
if 1==0:
    #plt.ioff()
    maanden = ['januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december',]
    for n in range(0,12):
        fig1, ax1 = plt.subplots(figsize=(10,10))
        
        if 1==0:
            #Ea:    figpath = '/home/baracus/Documents/Werk/Verdampingsdata/Figures/'

            data = Easum[:,:,n]
            data[data<-1]=None
            imgplot = plt.imshow(data)
            cb = plt.colorbar()
            cb.ax.tick_params(labelsize=18)
            plt.clim(0,100)
            imgplot.set_cmap('RdYlGn')   
            ax1.set_ylabel('', color='b')
            ax1.set_xlabel('')
            ax1.axes.get_xaxis().set_ticks([])
            ax1.axes.get_yaxis().set_ticks([])
            plt.title('Maandsom Ea '+maanden[n] + ' [mm/maand]', fontsize=22)
            plt.tight_layout()        
            plt.savefig(figpath+'maandom_ea_vast'+str(n+1)+'.png',format='png', dpi=300)
            plt.close()
        
        if 1==0:
        #Ed
            data = Edsum[:,:,n]
            data[data<-1]=None
            imgplot = plt.imshow(data)
            cb = plt.colorbar()
            cb.ax.tick_params(labelsize=18)
            plt.clim(0,15)
            imgplot.set_cmap('RdYlGn')   
            ax1.set_ylabel('', color='b')
            ax1.set_xlabel('')
            ax1.axes.get_xaxis().set_ticks([])
            ax1.axes.get_yaxis().set_ticks([])
            plt.title('Maandsom Ed '+maanden[n] + ' [mm/maand]', fontsize=22)
            plt.tight_layout()        
            plt.savefig(figpath+'maandom_ed_vast'+str(n+1)+'.png',format='png', dpi=300)
            plt.close()
        if 1==1:
        #Er
            data = np.flipud(Emsum[:,:,n])
            data[data<-1]=None
            imgplot = plt.imshow(data)
            cb = plt.colorbar()
            cb.ax.tick_params(labelsize=18)
            #plt.clim(0,15)
            imgplot.set_cmap('RdYlGn')   
            ax1.set_ylabel('', color='b')
            ax1.set_xlabel('')
            ax1.axes.get_xaxis().set_ticks([])
            ax1.axes.get_yaxis().set_ticks([])
            plt.title('Maandsom Er '+maanden[n] + ' [mm/maand]', fontsize=22)
            plt.tight_layout()        
            plt.savefig(figpath+'maandom_er_los'+str(n+1)+'.png',format='png', dpi=300)
            plt.close()

#Plots jaarsommen (Som, minimum, maximum)
if 1==0:  
    
    #plt.ioff()
    fig1, ax1 = plt.subplots(figsize=(10,10))
    #Som Ea
    if False:
        if 1==0:
            Easumy[Easumy<=-998]=None
            imgplot = plt.imshow(Easumy)
        #Min:
        if 1==0:
            Eamin = Ea.min(2)
            Eamin[Eamin<=-998]=None
            imgplot = plt.imshow(Eamin)    
        #Max:
        if 1==1:
            Eamax = Ea.max(2)
            Eamax[Eamax<=-998]=None
            imgplot = plt.imshow(Eamax)    

    #Som Ed
    if True:
        if 1==1:
            imgplot = plt.imshow(Edsumy)
        #Min:
        if 1==0:
            Edmin = Ed.min(2)
            Edmin[Edmin<=-998]=None
            imgplot = plt.imshow(Edmin)    
        #Max:
        if 1==0:
            Edmax = Ed.max(2)
            Edmax[Edmax<=-998]=None
            imgplot = plt.imshow(Edmax)    
        
    cb = plt.colorbar()
    plt.clim(0,70)
    cb.ax.tick_params(labelsize=18)
    imgplot.set_cmap('RdYlGn')   
    ax1.set_ylabel('', color='b')
    ax1.set_xlabel('')
    ax1.axes.get_xaxis().set_ticks([])
    ax1.axes.get_yaxis().set_ticks([])
    plt.title('Jaarsom Ed [mm]', fontsize=22)
    plt.tight_layout()   
    plt.savefig(figpath+'jaarsom_ed.png',format='png', dpi=300)

#EXPORTEER GeoTIFF
if 1==0:
    #array = np.flipud(Emsum[:,:,4])
    for n in range(0,5):
        array = Edweek[:,:,n]
        xmin,ymin,xmax,ymax = [x1.min(),y1.min(),x1.max(),y1.max()]
        nrows,ncols = np.shape(array)
        xres = (xmax-xmin)/float(ncols)
        yres = (ymax-ymin)/float(nrows)
        geotransform=(xmin,xres,0,ymax,0, -yres)   
        # That's (top left x, w-e pixel resolution, rotation (0 if North is up), 
        #         top left y, rotation (0 if North is up), n-s pixel resolution)
        # I don't know why rotation is in twice???
        
        output_raster = gdal.GetDriverByName('GTiff').Create('Export/Edweek_'+str(n)+'.tif',ncols, nrows, 1 ,gdal.GDT_Float32)  # Open the file
        output_raster.SetGeoTransform(geotransform)  # Specify its coordinates
        srs = osr.SpatialReference()                 # Establish its coordinate encoding
        srs.ImportFromEPSG(28992)                     # This one specifies WGS84 lat long.
        #srs.ImportFromEPSG(32631)                     # This one specifies WGS84 lat long.
    
        output_raster.SetProjection( srs.ExportToWkt() )   # Exports the coordinate system 
                                                           # to the file
        output_raster.GetRasterBand(1).WriteArray(array)   # Writes my array to the rasters
        
        output_raster= None
    
#Basemap kaartje Nijmegen
if 1==0:
    fig1, ax1 = plt.subplots(figsize=(10,10))
    IN = Proj(init='epsg:32631')        #WGS 84, lat/lon, transverse mercator 31n
    OU = Proj(init='epsg:4326')        
    xj,yj = np.meshgrid(x1,y1)
    lon,lat = transform(IN,OU,xj,yj)
    
    lat_0 = 51.75
    lon_0 = 5.969
    m = Basemap(width=30000,height=30000,resolution='l',projection='stere',lat_ts=40,lat_0=lat_0,lon_0=lon_0)
    xi,yi = m(lon,lat)
    data = Easumy
    data[data==-9999]=None
    
    cmap = plt.get_cmap('RdYlGn')
    cs = m.pcolor(xi,yi,data,cmap=cmap)  
    plt.clim(0,600)
    plt.title('Jaarsom Ea regio Nijmegen [mm]', fontsize=22)
    cbar = m.colorbar(cs, location = 'bottom')
    cbar.ax.tick_params(labelsize=18) 
    plt.tight_layout()
    plt.savefig(figpath+'beekendal.png',format='png', dpi=300)
    
#Basemap kaartje Friesland
if 1==0:
    fig1, ax1 = plt.subplots(figsize=(10,10))
    IN = Proj(init='epsg:32631')        #WGS 84, lat/lon, transverse mercator 31n
    OU = Proj(init='epsg:4326')        
    xj,yj = np.meshgrid(x1,y1)
    lon,lat = transform(IN,OU,xj,yj)
    
    lat_0 = 53.099524
    lon_0 = 5.752355
    m = Basemap(width=100000,height=75000,resolution='l',projection='stere',lat_ts=40,lat_0=lat_0,lon_0=lon_0)
    xi,yi = m(lon,lat)
    
    data = Easum[:,:,5] #Juni
    data[data==-9999]=None
    
    cmap = plt.get_cmap('RdYlGn')
    cs = m.pcolor(xi,yi,data,cmap=cmap)  
    plt.clim(0,90)
    plt.title('Maandsom Ea juni regio Friesland [mm]', fontsize=22)
    cbar = m.colorbar(cs, location = 'bottom')
    cbar.ax.tick_params(labelsize=18) 
    plt.tight_layout()  
    plt.savefig(figpath+'friesland.png',format='png', dpi=300)
    
    
    
    