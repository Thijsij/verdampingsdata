# -*- coding: utf-8 -*-
#
# Thijs IJpelaar, 23-06-2017

from __future__ import division
import numpy as np
import h5py
import os
from mpl_toolkits.basemap import Basemap


#Aan te pasen variabelen
#Invoerpaden:
basedir = "/home/baracus/Documents/Werk/Verdampingsdata/Data/Neerslag/2016/"
#Uitvoerpaden:
fui = '/home/baracus/Documents/Werk/Verdampingsdata/Data/Neerslag/neerslag2016_H5.h5'


n=0
dirlist = os.listdir(basedir)
prec=np.ndarray(shape=(765, 700, 366))

for file in sorted(dirlist):
    if os.path.splitext(file)[1] == '.h5':
        try:
            with h5py.File(basedir+file,'r') as hf:
                
                P = hf['image1']['image_data'][:]
                
                prec[:,:,n] = P * 0.01   #Conversiefactor, zie hieronder                
                #~['image1']['calibration'].attrs.values() = array(['GEO = 0.010000 * PV + 0.000000']
                
        except:                
            print "Inlezen lukt niet voor ",file
            P = np.empty([765, 700])
        n=n+1

#Nodig om te plotten als test:
'''
#'+proj=stere +lat_0=90 +lon_0=0 +lat_ts=60 +a=6378.14 +b=6356.75 +x_0=0 y_0=0'
map = Basemap(projection='stere', lat_0=90, lat_ts=60, lon_0=0, resolution='i',
              llcrnrlon=0,
              llcrnrlat=49.36206055,
              urcrnrlon=10.8564291,
              urcrnrlat=55.38897324)

x = np.linspace(map.llcrnrx,map.urcrnrx,prec[:,:,0].shape[1])
y = np.linspace(map.llcrnry,map.urcrnry,prec[:,:,0].shape[0])

xx,yy = np.meshgrid(x,y)
lat,lon=map(xx,yy,inverse=True)

Pcal = np.flipud(Pcal)
map.pcolormesh(xx,yy,Pcal)
map.drawcoastlines()
'''

#Uitschrijven data
with h5py.File(fui, 'w') as hf:
    hf.create_dataset("prec",  data=prec)
    hf.create_dataset("lat",  data=lat)
    hf.create_dataset("lon",  data=lon)
