# -*- coding: utf-8 -*-
#
# Thijs IJpelaar, 23-06-2017

from __future__ import division
import numpy as np
import os
import h5py
from netCDF4 import Dataset


#Aan te pasen variabelen
#Invoerpaden
fea = '/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/Ea/'
fed = '/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/Ed/'
feo = '/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/Eo/'
feq = '/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/Eq/'
#Uitvoerpaden:
fui = '/home/baracus/Documents/Werk/Verdampingsdata/Data/eLeaf 2016/eLeaf2016.h5'


#Inlezen Ea-bestanden
na=0
Ea = np.empty([1309,1049,366])
ta =  np.empty([366])
if 1==1:
    for doc in sorted(os.listdir(fea)):
        if doc.endswith('.nc'):
            fh = Dataset(fea+doc,mode='r')   
            ta[na] = fh.variables['time'][:]
            Ea[:,:,na] = fh.variables['Actual Evapotranspiration'][:][:,:,0]
            xa = fh.variables['x'][:]
            ya = fh.variables['y'][:]
            fh.close()
        na = na+1    
    print 'Ea klaar'
  
#Inlezen Ed-bestanden
nd=0
Ed = np.empty([1309,1049,366])
td =  np.empty([366])   
if 1==1:
    for doc in sorted(os.listdir(fed)):
        if doc.endswith('.nc'):
            fh = Dataset(fed+doc,mode='r')   
            td[nd] = fh.variables['time'][:]
            Ed[:,:,nd] = fh.variables['Evapotranspiration Deficit'][:][:,:,0]
            xd = fh.variables['x'][:]
            yd = fh.variables['y'][:]
            fh.close()
        nd = nd+1 
    print 'Ed klaar'
        
#Inlezen Eo-bestanden
no=0
Eo = np.empty([1309,1049,366])
to =  np.empty([366])   
if 1==1:
    for doc in sorted(os.listdir(feo)):
        if doc.endswith('.nc'):
            fh = Dataset(feo+doc,mode='r')   
            to[no] = fh.variables['time'][:]
            Eo[:,:,no] = fh.variables['Days since last observation'][:][:,:,0]
            xo = fh.variables['x'][:]
            yo = fh.variables['y'][:]
            fh.close()
        no = no+1 
    print 'Eo klaar'

#Inlezen Eq-bestanden
nq=0
Eq = np.empty([1309,1049,366])
tq =  np.empty([366])   
if 1==1:
    for doc in sorted(os.listdir(feq)):
        if doc.endswith('.nc'):
            fh = Dataset(feq+doc,mode='r')   
            tq[nq] = fh.variables['time'][:]
            Eq[:,:,nq] = fh.variables['Quality'][:][:,:,0]
            xq = fh.variables['x'][:]
            yq = fh.variables['y'][:]
            fh.close()
        nq = nq+1  
    print 'Eq klaar'

#Sla deze data op in een h5-bestand
with h5py.File(fui, 'w') as hf:
    hf.create_dataset("Ea",  data=Ea)
    hf.create_dataset("xa",  data=xa)
    hf.create_dataset("ya",  data=ya)
    hf.create_dataset("ta",  data=ta)
    print 'Ea geschreven'
    
    hf.create_dataset("Ed",  data=Ed)
    hf.create_dataset("xd",  data=xd)
    hf.create_dataset("yd",  data=yd)
    hf.create_dataset("td",  data=td)  
    print 'Ed geschreven'

    hf.create_dataset("Eo",  data=Eo)
    hf.create_dataset("xo",  data=xo)
    hf.create_dataset("yo",  data=yo)
    hf.create_dataset("to",  data=to)  
    print 'Eo geschreven'

    hf.create_dataset("Eq",  data=Eq)
    hf.create_dataset("xq",  data=xq)
    hf.create_dataset("yq",  data=yq)
    hf.create_dataset("tq",  data=tq)
    print 'Eq geschreven'
